import "mapbox-gl/dist/mapbox-gl.css";
import "react-map-gl-geocoder/dist/mapbox-gl-geocoder.css";

import React from "react";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import MapView from "./containers/MapView";
import FridayView from "./containers/FridayView";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexDirection: "column",
    minHeight: "100vh"
  },
  container: {
    flex: 1
  },
  footer: {
    padding: theme.spacing(3, 2),
    marginTop: "auto",
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[200]
        : theme.palette.grey[800]
  }
}));

function App() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="relative">
        <Toolbar>
          <Typography variant="h6" color="inherit" noWrap>
            WMapuj
          </Typography>
        </Toolbar>
      </AppBar>
      <Container maxWidth="xl" className={classes.container}>
        <Router>
          <Switch>
            <Route path="/friday">
              <FridayView />
            </Route>
            <Route path="/">
              <MapView />
            </Route>
          </Switch>
        </Router>
      </Container>
      <footer className={classes.footer}>
        <Typography variant="h6" align="center" gutterBottom>
          ┬─┬ ノ( ゜-゜ノ)
        </Typography>
        <Typography variant="body2" color="textSecondary" align="center">
          {"Copyright 2020"}
        </Typography>
      </footer>
    </div>
  );
}

export default App;
