import React, { useImperativeHandle, forwardRef } from "react";
import ReactMapGL, {
  Popup,
  NavigationControl,
  ScaleControl,
  Marker
} from "react-map-gl";
import { makeStyles } from "@material-ui/core/styles";
import Geocoder from "react-map-gl-geocoder";

import config from "../config";
import PopupInfo from "./PopupInfo";
import Pins from "./Pins";
import Pin from "./Pin";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  }
}));

const navStyle = {
  position: "absolute",
  top: 0,
  left: 0,
  padding: "10px"
};

const scaleControlStyle = {
  position: "absolute",
  bottom: 36,
  left: 0,
  padding: "10px"
};

const Map = forwardRef(({ data, onUpdateMarker }, ref) => {
  const [viewport, setViewport] = React.useState({
    latitude: 51.1079,
    longitude: 17.0385,
    zoom: 12
  });
  const [marker, setMarker] = React.useState(null);

  const [popupInfo, setPopupInfo] = React.useState(null);
  const mapRef = React.useRef();
  const classes = useStyles();

  // please don't judge me, I tired...
  useImperativeHandle(ref, () => ({
    createMarker() {
      const { latitude, longitude } = viewport;
      setMarker({ latitude, longitude });
      if (onUpdateMarker) {
        onUpdateMarker({
          longitude,
          latitude
        });
      }
    },
    resetMarker() {
      setMarker(null);
    }
  }));

  const _onClickMarker = point => {
    setPopupInfo(point);
  };

  const _renderPopup = () => {
    return (
      popupInfo && (
        <Popup
          tipSize={5}
          anchor="top"
          longitude={popupInfo.longitude}
          latitude={popupInfo.latitude}
          closeButton={false}
        >
          <PopupInfo
            info={popupInfo}
            onDelete={() => {
              console.warn("NOT IMPLEMENTED (╯°□°）╯︵ ┻━┻");
            }}
            onClose={() => setPopupInfo(null)}
          />
        </Popup>
      )
    );
  };

  const handleGeocoderViewportChange = viewport => {
    const geocoderDefaultOverrides = { transitionDuration: 1000 };

    setViewport({
      ...viewport,
      ...geocoderDefaultOverrides
    });
  };

  const _onMarkerDragEnd = event => {
    setMarker({
      longitude: event.lngLat[0],
      latitude: event.lngLat[1]
    });
    if (onUpdateMarker) {
      onUpdateMarker({
        longitude: event.lngLat[0],
        latitude: event.lngLat[1]
      });
    }
  };

  return (
    <div className={classes.root}>
      <ReactMapGL
        ref={mapRef}
        className={classes.root}
        {...viewport}
        width="100%"
        height="80vh"
        mapStyle="mapbox://styles/mapbox/streets-v9"
        mapboxApiAccessToken={config.MAPBOX_ACCESS_TOKEN}
        onViewportChange={viewport => {
          setViewport(viewport);
        }}
      >
        <Pins data={data} onClick={_onClickMarker} />
        {marker ? (
          <Marker
            longitude={marker.longitude}
            latitude={marker.latitude}
            offsetTop={-20}
            offsetLeft={-10}
            draggable
            onDragEnd={_onMarkerDragEnd}
          >
            <Pin size={20} />
          </Marker>
        ) : null}
        <div style={navStyle}>
          <NavigationControl />
        </div>
        <div style={scaleControlStyle}>
          <ScaleControl />
        </div>
        <Geocoder
          mapRef={mapRef}
          onViewportChange={handleGeocoderViewportChange}
          mapboxApiAccessToken={config.MAPBOX_ACCESS_TOKEN}
          position="top-right"
        />
        {_renderPopup()}
      </ReactMapGL>
    </div>
  );
});

export default Map;
