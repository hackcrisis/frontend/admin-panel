import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  root: {
    minWidth: 275
  }
});

export default function PopupInfo({ onClose, onDelete }) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CardContent>
        <h2>8:00-8:23 Fri, 11</h2>
      </CardContent>
      <CardActions>
        <Button size="small" color="secondary" onClick={onDelete}>
          Delete
        </Button>
        <Button size="small" onClick={onClose}>
          Close
        </Button>
      </CardActions>
    </div>
  );
}
