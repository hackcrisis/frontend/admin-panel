import React from "react";
import Grid from "@material-ui/core/Grid";

export default function() {
  return (
    <Grid container justify="space-around">
      <iframe
        src="https://giphy.com/embed/Udg5BOrPsKCQw"
        width="480"
        height="412"
        frameBorder="0"
        class="giphy-embed"
        allowFullScreen
      ></iframe>
    </Grid>
  );
}
