import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import Button from "@material-ui/core/Button";
import clsx from "clsx";

import Map from "../components/Map";
import DatePicker from "../components/DatePicker";

const useStyles = makeStyles(theme => ({
  root: {
    flex: 1
  },
  row: {
    marginTop: 20
  }
}));

const data = [
  {
    latitude: 51.1079,
    longitude: 17.0385,
    startDate: new Date(),
    endDate: new Date()
  }
];

export default function() {
  const classes = useStyles();
  const [latLong, setLatLong] = React.useState(null);
  const [eventDate, setEventDate] = React.useState(null);
  const mapRef = React.useRef();
  const dateRef = React.useRef();

  const sendToApi = () => {
    const { latitude, longitude } = latLong;
    const { startDate, endDate } = eventDate;

    data.push({
      latitude,
      longitude,
      startDate,
      endDate
    });

    setLatLong(null);
    setEventDate(null);
    if (dateRef) {
      dateRef.current.reset();
      mapRef.current.resetMarker();
    }
    console.warn("NOT IMPLEMENTED (╯°□°）╯︵ ┻━┻");
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={3} height="100%">
        <Grid item xs={8} height="100%">
          <Map
            ref={mapRef}
            data={data}
            onUpdateMarker={latLong => {
              setLatLong(latLong);
            }}
          />
        </Grid>
        <Grid item xs={4}>
          <Grid container>
            <h2>Create new entry</h2>
          </Grid>
          <Grid container justify="space-around">
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                if (mapRef) {
                  mapRef.current.createMarker();
                }
              }}
            >
              Create marker
            </Button>
            <TextField
              id="standard-start-adornment"
              value={
                latLong
                  ? `${latLong.longitude.toFixed(
                      3
                    )}° ${latLong.latitude.toFixed(3)}°`
                  : ""
              }
              className={clsx(classes.margin, classes.textField)}
              InputProps={{
                readOnly: true,
                startAdornment: (
                  <InputAdornment position="start">Lat/Long</InputAdornment>
                )
              }}
            />
          </Grid>
          <DatePicker
            ref={dateRef}
            onChange={(startDate, endDate) => {
              setEventDate({ startDate, endDate });
            }}
          />
          <Grid container className={classes.row} justify="space-around">
            <Button
              variant="contained"
              color="primary"
              onClick={sendToApi}
              disabled={!eventDate || !latLong}
            >
              Add entry
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}
